package pl.edu.pwsztar;

import java.util.List;

public interface FileXmlGenerator {
    void toXml(List<String> lines);
}
