package pl.edu.pwsztar;

import java.util.List;

public interface FilePdfGenerator {
    void toPdf(List<String> lines);
}
