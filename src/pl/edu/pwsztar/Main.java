package pl.edu.pwsztar;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        FileCsvGenerator fileGenerator = new CsvGenerator();

        fileGenerator.toCsv(Arrays.asList("first line", "second line", "third line"));

    }

}
