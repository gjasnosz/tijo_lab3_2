package pl.edu.pwsztar;

import java.io.IOException;
import java.util.List;

public interface FileCsvGenerator {
    void toCsv(List<String> lines);
}
