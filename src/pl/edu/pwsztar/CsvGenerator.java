package pl.edu.pwsztar;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvGenerator implements FileCsvGenerator {


    @Override
    public void toCsv(List<String> lines) {

        final String STARS = "*************";
        try {
            FileWriter csvFile = new FileWriter("new.csv");

            createLine(csvFile, STARS);

            for (String line : lines) {
                createLine(csvFile, line);
            }

            createLine(csvFile, STARS);

            csvFile.flush();
            csvFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void createLine(FileWriter csv, String line) throws IOException {
        csv.append(line);
        csv.append("\n");
    }


}
